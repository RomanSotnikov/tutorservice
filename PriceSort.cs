﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tutorService
{
    public class PriceSort : IComparer<Service>
    {
        public int Compare(Service o1, Service o2)
        {
            if (o1.Cost > o2.Cost)
            {
                return 1;
            }
            else if (o1.Cost < o2.Cost)
            {
                return -1;
            }

            return 0;
        }
    }
}
