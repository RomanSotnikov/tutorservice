﻿namespace tutorService
{
    public class Service
    {
        private float _discount;
        public int Id { get; set; }

        public string Title { get; set; }

        public int Cost { get; set; }

        public int DurationInSeconds { get; set; }

        public float Discount
        {
            get
            {
                return _discount * Cost;
            }
            set
            {
                _discount = value;
            }
        }

        public byte[] Photo { get; set; }
    }
}
