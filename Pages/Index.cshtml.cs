﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Threading.Tasks;

namespace tutorService.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public readonly List<Service> Services;

        public IndexModel(ILogger<IndexModel> logger, IConfiguration configuration)
        {
            _logger = logger;
            SqlConnection sql = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            
            /*Services = new List<Service>();
            for (int i = 0; i < 10; i++)
            {
                Service service = new Service
                {
                    Id = i,
                    Title = "Услуга №" + i,
                    Cost = i + 1000,
                    DurationInSeconds = i + 10000,
                    Discount = (i + 1) / 10
                };

                Services.Add(service);
            }*/

            Services = sql.Query<Service>("exec select_from_service").ToList();

            //PriceSort priceSort = new PriceSort();
            //Services.Sort(priceSort);
        }

        public void OnGet()
        {

        }
    }
}
